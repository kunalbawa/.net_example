﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OdeToFood.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class RestaurantReview
    {
        public int Id { get; set; }
        public string Rating { get; set; }
        public string Body { get; set; }
        public int RestaurantId { get; set; }
    }
}