﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace OdeToFood.Models
{
    public class OdeDb : DbContext
    {
        public DbSet<RestaurantReview> Reviews { get; set; }
        public DbSet<Restaurant> Restaurants { get; set; }
    }
}