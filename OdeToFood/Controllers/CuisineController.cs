﻿using OdeToFood.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OdeToFood.Controllers
{
    // [Authorize] MAke sure you logged in
    [Log]
    public class CuisineController : Controller
    {
        //
        // GET: /Cuisine/

        public ActionResult Index()
        {
            return View();
        }

        //// this means that name has a default value french if nothing is passed
        //// But not a good example as you should never inspect the route data in 
        //// the controller
        //// only called on a post request
        //[HttpPost]
        //public ActionResult Search(String name = "french") 
        //{
        //    var message = Server.HtmlEncode(name);
        //    return Content(message);
        //}
        //// only called on a get request
        //[HttpGet]
        //public ActionResult Search() 
        //{
        //    return Content("Search!");
        //}

        // [Authorize] make sure you logged in 
        public ActionResult Search(String name = "french") 
        {

            throw new Exception("Something wrong has happened");

            var message = Server.HtmlEncode(name);
            return Content(message);
        }
    }
}
