﻿using OdeToFood.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OdeToFood.Controllers
{
    public class HomeController : Controller
    {
        OdeDb _db = new OdeDb();
        public ActionResult Index()
        {
            var model = _db.Restaurants.ToList();
            // Bad exmaple as MVC does it for you automatically
            //var controller = RouteData.Values["controller"];
            //var action = RouteData.Values["action"];
            //var id = RouteData.Values["id"];

            //var message = String.Format("{0}::{1} {2}",controller,action,id);

            ViewBag.Message = "Modify this template to jump-start your ASP.NET MVC application.";
            //ViewBag.Message = message;


            return View(model);
        }

        public ActionResult About()
        {
            var model = new AboutModel();
            model.Name = "Kunal";
            model.Location = "India";
            return View(model);
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        protected override void Dispose(bool disposing)
        {
            if (_db != null)
            {
                _db.Dispose();
            }    
            base.Dispose(disposing);
        }
    }
}
